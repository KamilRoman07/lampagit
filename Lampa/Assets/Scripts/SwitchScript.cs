﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class SwitchScript : MonoBehaviour
{
    [SerializeField] Material switchMaterialOn;
    [SerializeField] Material switchMaterialOff;
    [SerializeField] Light lightBulb;

    private bool on = false;
    // Start is called before the first frame update
    void Start()
    {
        lightBulb.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        on = !on;
        switchMaterial();
    }

    public void switchMaterial()
    {
        lightBulb.enabled = !lightBulb.enabled;
        if (on)
        {
            gameObject.GetComponent<Renderer>().material = switchMaterialOn;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material = switchMaterialOff;
        }
    }
}
